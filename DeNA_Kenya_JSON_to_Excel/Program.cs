﻿using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DeNA_Kenya_JSON_to_Excel
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("copy and paste JSON");
            string @jsonfile = Console.ReadLine();

            jsonfile = jsonfile.Replace("\"", "");

            string[] lines = File.ReadAllLines(jsonfile);

            int count = 0;
            

            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets[0];


            ws.Cells[count, 0].Value = "Language";
            ws.Cells[count, 1].Value = "location";
            ws.Cells[count, 2].Value = "text";
            ws.Cells[count, 3].Value = "message";
            ws.Cells[count, 4].Value = "type";
            ws.Cells[count, 5].Value = "comment";

            count++;

            string lang = "";
            string location = "";
            string text = "";
            string message = "";
            string type = "";
            string comment = "";


            for (int i = 0; i < lines.Length; i++)
            {
                Match Lang = Regex.Match(lines[i], "^.*?\"lang\": \"(.*?)\",$");
                if (Lang.Success)
                {
                    lang = Regex.Match(lines[i], "^.*?\"lang\": \"(.*?)\",$").Groups[1].ToString();
                }

                Match Location = Regex.Match(lines[i], "^.*?\"location\": \"(.*?)\",$");
                if (Location.Success)
                {
                    location = Regex.Match(lines[i], "^.*?\"location\": \"(.*?)\",$").Groups[1].ToString();
                }

                Match Text = Regex.Match(lines[i], "^.*?\"text\": \"(.*?)\",$");
                if (Text.Success)
                {
                    text = Regex.Match(lines[i], "^.*?\"text\": \"(.*?)\",$").Groups[1].ToString();
                }

                Match Message = Regex.Match(lines[i], "^.*?\"message\": \"(.*?)\",$");
                if (Message.Success)
                {
                    message = Regex.Match(lines[i], "^.*?\"message\": \"(.*?)\",$").Groups[1].ToString();
                }
                
                Match Type = Regex.Match(lines[i], "^.*?\"type\": \"(.*?)\",$");
                if (Type.Success)
                {
                    type = Regex.Match(lines[i], "^.*?\"type\": \"(.*?)\",$").Groups[1].ToString();
                }
                

                Match Comment = Regex.Match(lines[i], "^.*?\"comment\": \"(.*?)\"$");
                if (Comment.Success)
                {
                    
                    comment = Regex.Match(lines[i], "^.*?\"comment\": \"(.*?)\"$").Groups[1].ToString();
                    

                    Console.WriteLine($"Line = {i}\nCount = {count}\nLanguage = {lang}\nLocation = {location}\nText = {text}\nMessage = {message}\nType = {type}\nComment = {comment}\n--------");
                    
                    ws.Cells[count, 0].Value = lang;
                    ws.Cells[count, 1].Value = location;
                    ws.Cells[count, 2].Value = text.Replace("\\", ""); 
                    ws.Cells[count, 3].Value = message.Replace("\\","");
                    ws.Cells[count, 4].Value = type;
                    ws.Cells[count, 5].Value = comment;


                    lang = "";
                    location = "";
                    text = "";
                    message = "";
                    type = "";
                    comment = "";

                    count++;
                }

            }

            wb.Save(jsonfile.Replace(".json", ".xlsx"));
            Console.WriteLine("Done");
            Console.ReadKey();

        }

        class json
        {
            private int id;
            private string lang;
            private string location;
            private string text;
            private string type;
            private string comment;

            public json(int id, string lang, string location, string text, string type, string comment)
            {
                this.id = id;
                this.lang = lang;
                this.location = location;
                this.text = text;
                this.type = type;
                this.comment = comment;
            }

            public int Id
            {
                get { return id; }
            }
            public string Lang
            {
                get { return lang; }
            }
            public string Location
            {
                get { return location; }
            }
            public string Text
            {
                get { return text; }
            }
            public string Type
            {
                get { return type; }
            }
            public string Comment
            {
                get { return comment; }
            }
        }

    }
}

